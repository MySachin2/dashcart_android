package com.phacsin.dashcart.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.phacsin.dashcart.R;
import com.phacsin.dashcart.adapters.CategoryAdapter;
import com.phacsin.dashcart.models.Category;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CategoryFragment extends Fragment implements Step {
    RecyclerView recyclerView;
    List<Category> lstCategory ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.launch_store_step_1, container, false);
        recyclerView = v.findViewById(R.id.recyclerview_categories);
        lstCategory = new ArrayList<>();
        lstCategory.add(new Category("The Vegitarian",R.mipmap.ic_launcher));
        lstCategory.add(new Category("The Wild Robot",R.mipmap.ic_launcher));
        lstCategory.add(new Category("Maria Semples",R.mipmap.ic_launcher));
        lstCategory.add(new Category("Maria Semples",R.mipmap.ic_launcher));
        CategoryAdapter myAdapter = new CategoryAdapter(getActivity(),lstCategory);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3));
        recyclerView.setAdapter(myAdapter);
        return v;
    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }
    
}