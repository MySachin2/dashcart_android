package com.phacsin.dashcart.adapters;

import android.content.Context;
import android.os.Bundle;

import com.phacsin.dashcart.R;
import com.phacsin.dashcart.fragments.CategoryFragment;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;

import androidx.fragment.app.FragmentManager;

public class MyStepperAdapter extends AbstractFragmentStepAdapter {

    public MyStepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        final CategoryFragment step = new CategoryFragment();
        return step;
    }

    @Override
    public int getCount() {
        return 3;
    }

}