package com.phacsin.dashcart.models;

public class Store {

    private String title,description,url;
    private int thumbnail ;

    public Store(String title, String description, String url, int thumbnail) {
        this.title = title;
        this.description = description;
        this.url = url;
        this.thumbnail = thumbnail;
    }


    public String getTitle() {
        return title;
    }


    public int getThumbnail() {
        return thumbnail;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}