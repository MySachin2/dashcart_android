package com.phacsin.dashcart.models;

public class Category {

    private String title;
    private int thumbnail ;

    public Category() {
    }

    public Category(String title, int thumbnail) {
        this.title = title;
        this.thumbnail = thumbnail;
    }


    public String getTitle() {
        return title;
    }


    public int getThumbnail() {
        return thumbnail;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}